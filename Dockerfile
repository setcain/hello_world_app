FROM python:3.7-alpine
LABEL maintainer="setcain <setcain00@gmail.com>"

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /app

COPY Pipfile Pipfile.lock /app/
RUN pip install --no-cache-dir pipenv && \
    pipenv install --system

COPY . /app

RUN adduser -D user
USER user
