from django.urls import path
from . import views as pages_views

urlpatterns = [
    path('', pages_views.home, name='home_url'),
]
