from django.http import HttpResponse


def home(request):
    """
    Return hello world in /
    """
    return HttpResponse('Hello world')
